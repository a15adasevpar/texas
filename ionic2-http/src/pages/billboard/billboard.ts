import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { NavParams } from 'ionic-angular';

import { FilmPage } from '../film/film';
import { TexasService } from '../../app/texas.service';

@Component({
  selector: 'page-billboard',
  templateUrl: 'billboard.html'
})
export class BillboardPage implements OnInit{
   
	posts: any;	
	myDate: Date;
	myDateString: String;
	filmsIDs: String[];
	arrTodaySessions : String[] = [];
	

	constructor(private navCtrl: NavController, public navParams: NavParams, public http: Http, private texasService: TexasService ) {
		
		/*this.http.get(texasService.getTexasUrl()+'pelis2.json').map(res => res.json()).subscribe(data => {
        this.posts = data;
    	});*/
    	this.http.get('http://labs.iam.cat:9015/api').map(res => res.json()).subscribe(data => {
        this.posts = data;
    	});
	    	
		this.myDate = new Date();
		this.myDateString = this.texasService.beautifyDate(this.myDate);
		
		//console.log(Object.getOwnPropertyNames(this.navParams));
		//console.log(Object.getOwnPropertyNames(this.navParams).length === 0);
		//console.log(this.navParams.toString());
		//console.log(this.navParams.toString() == '[object Object]');
	    //this.myDateString = this.texasService.dateStringFormat(this.myDate);
 	}

	ngOnInit(){
		//selectTodaySessions();
		
	}

	/* AFEGEIX A L'ARRAY LES SESSIONS QUE SÓN D'AVUI ÚNICAMENT, NO FUNCIONA
	selectTodaySessions () {
		for ( let session of this.posts ) {
			console.log(session);
		}
	}
	*/
	
	passToNextDay() {

		//this.myDate.setDate(this.myDate.getDate()+1);
		//this.texasService.myDate.setDate(this.myDate.getDate()+1);		
		
		//this.myDateString = this.texasService.dateStringFormat(this.myDate);
		//console.log(this.texasService.myDate);
		
		//location.reload();		
		//this.navCtrl.push(BillboardPage, this.myDate);
		
		//window.history.pushState(this.myDate);
		location.reload();
		//document.getElementById("ion-content").load();
	}
	
	addId(filmID) {
		if (typeof this.filmsIDs != 'undefined') {
			if (this.filmAlreadyExists(filmID)) {
				//this.filmsIDs.push(filmID);
				//this.filmsIDs[0] = filmID;
			}else {
				console.log("film already exists");
			}
		} else {
			//this.filmsIDs.push(filmID);
			//this.filmsIDs[0] = filmID;		
		}

	}
	
	filmAlreadyExists(currentFilmID) {
		var exists = false;		
		for (let filmID of this.filmsIDs) {
			if (filmID == currentFilmID) {
				exists = true;
			}
		}
		console.log("exists: " + exists);
		return exists;
	}
	
	generateSessions(session) {
		var id = session.billboar.film.id;
		if (this.filmAlreadyExists(id)) {
			//afegir sessió. Amb dom afegir: 
			//document.getElementById(id).appendChild("");	
		}
		else {
			//Crear nou box		
		}
	}

	goToFilmPage(post) {
		this.navCtrl.push(FilmPage, post);
	}
	
	getActualWeek() {
		var actualDate = new Date();
	    var onejan = new Date(actualDate.getFullYear(),0,1);
	    var today = new Date(actualDate.getFullYear(),actualDate.getMonth(),actualDate.getDate());
	    var dayOfYear = ((Number(today) - Number(onejan) +1)/86400000);
	    return Math.ceil(dayOfYear/7);
	}
	
	getWeek(date) {
		var dateGiven = new Date(date);
	    var onejan = new Date(dateGiven.getFullYear(),0,1);
		var today = new Date(dateGiven.getFullYear(),dateGiven.getMonth(),dateGiven.getDate());
		var dayOfYear = ((Number(today) - Number(onejan) +1)/86400000);
		return Math.ceil(dayOfYear/7);
	}

	dateIsInThisWeek(date) {
		if (this.getWeek(date) == this.getActualWeek())	{
			return true;
		} else {
			return false;
		}
	}
	/*sessionIsToday(sessionDate) {
		var dateAux = this.myDate;
		return true;
	}*/
	getImageURL(imageURL) {
		return "http://labs.iam.cat:9015/uploads/imageLogos/" + imageURL;
	}

	/*
	doRefresh(refresher) {
		refresher.go(refresher.current, {}, {reload:true});
		setTimeout(() => {
			refresher.complete();
		}, 1000);
		
	}*/

}
