import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
 
@Component({
  selector: 'page-info',
  templateUrl: 'info.html'
})
export class InfoPage {
 
  constructor(public navCtrl: NavController, public http: Http) {
 
  }
}
