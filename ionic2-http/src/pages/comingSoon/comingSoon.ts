import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { FilmPage} from '../film/film';
 
@Component({
  selector: 'page-comingSoon',
  templateUrl: 'comingSoon.html'
})
export class ComingSoonPage {
 
  posts: any;
 
  constructor(public navCtrl: NavController, public http: Http) {

    this.http.get('http://labs.iam.cat/~a15adasevpar/properament.json').map(res => res.json()).subscribe(data => {
        this.posts = data.results;
    });
 
  }
  
  goToFilmPage(post) {
      /*
      this.navCtrl.push(FilmPage, {
        filmData: post
      });
      */
      this.navCtrl.push(FilmPage, post)
    }
}
