import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import 'rxjs/add/operator/map';

import { NavParams } from 'ionic-angular';
import { FilmPage } from '../film/film';
@Component({
  selector: 'page-event',
  templateUrl: 'event.html'
})
export class EventPage {
 
  private allParams;

  constructor(private navCtrl: NavController, public navParams: NavParams) {
     this.allParams = this.navParams.data;
     console.log(this.allParams);
  }

  goBackToPreviousPage() {
    	this.navCtrl.pop();
    }
	
	eventIsACicle(post) {
  		if (post.type === "Cicle") {
			return true;  		
  		}
  		return false;
  }
  
  eventIsACineForum(post) {
  		if (post.type === "Cineforum") {
			return true;  		
  		}
  		return false;
  }
  
  convertDateFormat(date) {
  		var str = date;
  		var newStr = str.substring(3,5) + "/" + str.substring(0,2) + "/" + str.substring(6,10);
  		return newStr;
  }
  
	goToFilmPage(allParams) {
		this.navCtrl.push(FilmPage, allParams);  
  }

}

