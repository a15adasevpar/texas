import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { TexasService } from '../../app/texas.service';

import {EventPage} from '../event/event';
@Component({
  selector: 'page-events',
  templateUrl: 'events.html'
})
export class EventsPage {
 
  posts: any;

  arrCicles : any;
  arrCineforums : any;
  arrDefault : any;
 
  constructor(public navCtrl: NavController, public http: Http, private texasService: TexasService) {

    this.http.get('http://labs.iam.cat/~a15adasevpar/esdeveniments.json').map(res => res.json()).subscribe(data => {
        this.posts = data.results;
    });

    this.http.get('http://labs.iam.cat:9015/apiEsdevenimentsCicles').map(res => res.json()).subscribe(data => {
        this.arrCicles = data;
    });

    this.http.get('http://labs.iam.cat:9015/apiEsdevenimentsCineforum').map(res => res.json()).subscribe(data => {
        this.arrCineforums = data;
    });

    this.http.get('http://labs.iam.cat:9015/apiEsdevenimentsGeneric').map(res => res.json()).subscribe(data => {
        this.arrDefault = data;
    });
 
  }
  
  eventIsACicle(post) {
  		if (post.type === "Cicle") {
			return true;  		
  		}
  		return false;
  }
  
  eventIsACineForum(post) {
  		if (post.type === "Cineforum") {
			return true;  		
  		}
  		return false;
  }

	
  convertDateFormat(date) {
  		var str = date;
  		var newStr = str.substring(3,5) + "/" + str.substring(0,2) + "/" + str.substring(6,10);
  		return newStr;
  }
  
  filmHasImage(film) {
		console.log(Object.keys(film.image).length);		
		if (Object.keys(film.image).length != 0) {
			console.log("1");			
			return true;		
		}
		return false;
  }
  
  goToEventPage(post) {
		this.navCtrl.push(EventPage, post);  
  }
}
