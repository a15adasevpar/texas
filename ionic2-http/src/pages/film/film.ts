import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import 'rxjs/add/operator/map';

import { TexasService} from '../../app/texas.service';

import { NavParams } from 'ionic-angular';

@Component({
  selector: 'page-film',
  templateUrl: 'film.html'
})
export class FilmPage {
 
  private allParams;

  constructor(private navCtrl: NavController, public navParams: NavParams, private texasService : TexasService) {
     this.allParams = this.navParams.data;
     console.log(this.allParams);
  }

  goBackToPreviousPage() {
    	this.navCtrl.pop();
    }

  convertDateFormat(date) {
      var str = date;
      var newStr = str.substring(3,5) + "/" + str.substring(0,2) + "/" + str.substring(6,10);
      return newStr;
  }
  nextThreeDaysSessions(sessions) {
   

  }

}

