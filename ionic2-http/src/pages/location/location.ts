import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
 
@Component({
  selector: 'page-location',
  templateUrl: 'location.html'
})
export class LocationPage {
 
  posts: any;
 
  constructor(public navCtrl: NavController, public http: Http) {

    this.http.get('http://labs.iam.cat/~a15adasevpar/onsom.json').map(res => res.json()).subscribe(data => {
        this.posts = data;
    });
 
  }
}
