import { Directive } from '@angular/core';

/**
 * Generated class for the Billboard directive.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/DirectiveMetadata-class.html
 * for more info on Angular Directives.
 */
@Directive({
  selector: '[billboard]' // Attribute selector
})
export class Billboard {

  constructor() {
    console.log('Hello Billboard Directive');
  }

}
