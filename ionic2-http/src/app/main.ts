import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app.module';
import { TexasService } from './texas.service';

platformBrowserDynamic().bootstrapModule(AppModule, [TexasService]);
