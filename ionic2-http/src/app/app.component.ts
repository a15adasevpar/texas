import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { BillboardPage } from '../pages/billboard/billboard'
import { ComingSoonPage } from '../pages/comingSoon/comingSoon';
import { EventsPage } from '../pages/events/events';
import { LocationPage } from '../pages/location/location';
import { InfoPage } from '../pages/info/info';

@Component({
  selector: 'page-home',
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = BillboardPage;
  activePage: any;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Cartellera', component: BillboardPage },
      { title: 'Properament', component: ComingSoonPage },
      { title: 'Esdeveniments', component: EventsPage },
      { title: 'On Som', component: LocationPage },
      { title: 'Els Texas', component: InfoPage }
    ];

    this.activePage = this.pages[0];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
    this.activePage = page;
  }
  checkActive(page) {
    return page == this.activePage;
  }
}
