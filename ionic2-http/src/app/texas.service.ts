import { Component } from '@angular/core';


export class TexasService{
	
	private texasUrl = "http://labs.iam.cat/~a15adasevpar/";/*private texasUrl = "http://labs.iam.cat/~a15adasevpar/";*/
	private static pelisJSON=null;
	public  myDate = new Date() ;
	public  aux = null;
	
	constructor(){
		this.aux=0;
	}
	getTexasUrl(){
		return this.texasUrl;
	}
	
	incAux() {
		this.aux++;
		}
	getAux() {
		return this.aux;
	}
	
  dateStringFormat(date) {
  		return ("0" + date.getDate()).slice(-2).toString() + "/" + ("0" + (date.getMonth() + 1)).slice(-2).toString() + "/" + (date.getFullYear()).toString(); 
  }
	
	filmHasSessionsInDay(post) {
    var hasSessions = false;
    for (let session of post.sessions) {
      if (this.compareDates(session.date) == true) {
        hasSessions = true;
      }
    }
    return hasSessions;
  }

  compareDates(date1) {
    var dateObj1 = new Date(date1);
    var dateObj2 = new Date();
    var dateText1 = dateObj1.getDate() + "/" + dateObj1.getMonth() + "/" + dateObj1.getFullYear();
    var dateText2 = dateObj2.getDate() + "/" + dateObj2.getMonth() + "/" + dateObj2.getFullYear();
    return (dateText1 == dateText2);
  }
	
	beautifyDate(date) {
		var dateAux = new Date(date);
		return this.dateStringFormat(dateAux);
	}
	beautifyHour(date) {
		var dateAux = new Date(date);
		return dateAux.getHours() + ":" + ("0" + dateAux.getMinutes()).slice(-2).toString();
	}
	


}
