import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { NgModule, ErrorHandler } from '@angular/core';

import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

import { MyApp } from './app.component';

import { BillboardPage } from '../pages/billboard/billboard'
import { ComingSoonPage } from '../pages/comingSoon/comingSoon';
import { EventsPage } from '../pages/events/events';
import { LocationPage } from '../pages/location/location';
import { FilmPage } from '../pages/film/film';
import { EventPage } from '../pages/event/event';
import { InfoPage } from '../pages/info/info';
import { TexasService } from './texas.service';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
 
@NgModule({
  declarations: [
    MyApp,
    BillboardPage,
    ComingSoonPage,
    EventsPage,
    LocationPage,
    FilmPage,
    EventPage,
    InfoPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    BillboardPage,
    ComingSoonPage,
    EventsPage,
    LocationPage,
    FilmPage,
    EventPage,
    InfoPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
	TexasService
  ]
})
export class AppModule {}
