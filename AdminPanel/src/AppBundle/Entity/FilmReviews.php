<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FilmReviews
 *
 * @ORM\Table(name="film_reviews", indexes={@ORM\Index(name="film_id", columns={"film_id"})})
 * @ORM\Entity
 */
class FilmReviews
{
    /**
     * @var string
     *
     * @ORM\Column(name="review", type="string", length=300, nullable=true)
     */
    private $review;

    /**
     * @var integer
     *
     * @ORM\Column(name="reviewRating", type="integer", nullable=false)
     */
    private $reviewRating;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Film
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Film")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="film_id", referencedColumnName="id")
     * })
     */
    private $film;

    /**
     * Set review
     *
     * @param string $review
     *
     * @return FilmReviews
     */
    public function setReview($review)
    {
        $this->review = $review;

        return $this;
    }

    /**
     * Get review
     *
     * @return string
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * Set reviewRating
     *
     * @param integer $reviewRating
     *
     * @return FilmReviews
     */
    public function setReviewRating($reviewRating)
    {
        $this->reviewRating = $reviewRating;

        return $this;
    }

    /**
     * Get reviewRating
     *
     * @return integer
     */
    public function getReviewRating()
    {
        return $this->reviewRating;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return FilmReviews
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set film
     *
     * @param \AppBundle\Entity\Film $film
     *
     * @return FilmReviews
     */
    public function setFilm(\AppBundle\Entity\Film $film)
    {
        $this->film = $film;

        return $this;
    }

    /**
     * Get film
     *
     * @return \AppBundle\Entity\Film
     */
    public function getFilm()
    {
        return $this->film;
    }
    /**
     * @var integer
     */
    private $reviewrating;


}
