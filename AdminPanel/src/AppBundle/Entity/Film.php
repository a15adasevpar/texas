<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pelicules
 *
 * @ORM\Table(name="films")
 * @ORM\Entity
 */
class Film
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=120, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="overview", type="string", length=120, nullable=true)
     */
    private $overview;

    /**
     * @var string
     *
     * @ORM\Column(name="awards", type="string", length=120, nullable=true)
     */
    private $awards;

    /**
     * @var string
     *
     * @ORM\Column(name="originalTitle", type="string", length=120, nullable=true)
     */
    private $originalTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="nacionality", type="string", length=120, nullable=true)
     */
    private $nacionality;

    /**
     * @var integer
     *
     * @ORM\Column(name="productionYear", type="smallint", nullable=true)
     */
    private $productionYear;

    /**
     * @var string
     *
     * @ORM\Column(name="genre", type="string", length=120, nullable=true)
     */
    private $genre;

    /**
     * @var integer
     *
     * @ORM\Column(name="duration", type="smallint", nullable=true)
     */
    private $duration;

    /**
     * @var string
     *
     * @ORM\Column(name="directors", type="string", length=120, nullable=true)
     */
    private $directors;

    /**
     * @var string
     *
     * @ORM\Column(name="actors", type="string", length=120, nullable=true)
     */
    private $actors;

    /**
     * @var string
     *
     * @ORM\Column(name="version", type="string", length=120, nullable=true)
     */
    private $version;

    /**
     * @var integer
     *
     * @ORM\Column(name="rating", type="integer", nullable=true)
     */
    private $rating;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     # @ORM\Column(name="rate", type="integer", nullable=true)
     */
    private $rate;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=120, nullable=true)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="trailer", type="string", length=120, nullable=true)
     */
    private $trailer;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Film
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set overview
     *
     * @param string $overview
     *
     * @return Film
     */
    public function setOverview($overview)
    {
        $this->overview = $overview;

        return $this;
    }

    /**
     * Get overview
     *
     * @return string
     */
    public function getOverview()
    {
        return $this->overview;
    }

    /**
     * Set awards
     *
     * @param string $awards
     *
     * @return Film
     */
    public function setAwards($awards)
    {
        $this->awards = $awards;

        return $this;
    }

    /**
     * Get awards
     *
     * @return string
     */
    public function getAwards()
    {
        return $this->awards;
    }

    /**
     * Set originalTitle
     *
     * @param string $originalTitle
     *
     * @return Film
     */
    public function setOriginalTitle($originalTitle)
    {
        $this->originalTitle = $originalTitle;

        return $this;
    }

    /**
     * Get originalTitle
     *
     * @return string
     */
    public function getOriginalTitle()
    {
        return $this->originalTitle;
    }

    /**
     * Set nacionality
     *
     * @param string $nacionality
     *
     * @return Film
     */
    public function setNacionality($nacionality)
    {
        $this->nacionality = $nacionality;

        return $this;
    }

    /**
     * Get nacionality
     *
     * @return string
     */
    public function getNacionality()
    {
        return $this->nacionality;
    }

    /**
     * Set productionYear
     *
     * @param integer $productionYear
     *
     * @return Film
     */
    public function setProductionYear($productionYear)
    {
        $this->productionYear = $productionYear;

        return $this;
    }

    /**
     * Get productionYear
     *
     * @return integer
     */
    public function getProductionYear()
    {
        return $this->productionYear;
    }

    /**
     * Set genre
     *
     * @param string $genre
     *
     * @return Film
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * Get genre
     *
     * @return string
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     *
     * @return Film
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set directors
     *
     * @param string $directors
     *
     * @return Film
     */
    public function setDirectors($directors)
    {
        $this->directors = $directors;

        return $this;
    }

    /**
     * Get directors
     *
     * @return string
     */
    public function getDirectors()
    {
        return $this->directors;
    }

    /**
     * Set actors
     *
     * @param string $actors
     *
     * @return Film
     */
    public function setActors($actors)
    {
        $this->actors = $actors;

        return $this;
    }

    /**
     * Get actors
     *
     * @return string
     */
    public function getActors()
    {
        return $this->actors;
    }

    /**
     * Set version
     *
     * @param string $version
     *
     * @return Film
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     *
     * @return Film
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rate
     *
     * @param integer $rate
     *
     * @return Film
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return integer
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Film
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set trailer
     *
     * @param string $trailer
     *
     * @return Film
     */
    public function setTrailer($trailer)
    {
        $this->trailer = $trailer;

        return $this;
    }

    /**
     * Get trailer
     *
     * @return string
     */
    public function getTrailer()
    {
        return $this->trailer;
    }

  public function __toString()
    {
        return $this->name;
    }
}
