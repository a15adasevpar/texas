<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 *
 * @ORM\Table(name="event_cicles", indexes={ @ORM\Index(name="eventId", columns={"eventId"}), @ORM\Index(name="film", columns={"film"})})
 * @ORM\Entity
 */
class EventCicles
{
    /**
     * @var \AppBundle\Entity\Events
     *
	 * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Events")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="eventId", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $eventId;

    /**
     * @var \AppBundle\Entity\Film
	 
	 * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Film")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="film", referencedColumnName="id",onDelete="CASCADE")
     * })
     */
    private $film;


    /**
     * Set eventId
     *
     * @param \AppBundle\Entity\Events $eventId
     *
     * @return EventCicles
     */
    public function setEventId($eventId)
    {
        $this->eventId = $eventId;

        return $this;
    }

    /**
     * Get eventId
     *
     * @return integer
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * Set film
     *
     * @param \AppBundle\Entity\Film $film
     *
     * @return EventCicles
     */
    public function setFilm(\AppBundle\Entity\Film $film)
    {
        $this->film = $film;

        return $this;
    }

    /**
     * Get film
     *
     * @return \AppBundle\Entity\Film
     */
    public function getFilm()
    {
        return $this->film;
    }
}
