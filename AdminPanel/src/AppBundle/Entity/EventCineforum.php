<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 *
 * @ORM\Table(name="event_cineforums", indexes={ @ORM\Index(name="eventId", columns={"eventId"}), @ORM\Index(name="sessionId", columns={"sessionId"})})
 * @ORM\Entity
 */
class EventCineforum
{
    /**
     * @var \AppBundle\Entity\Events
     *
	 * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Events")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="eventId", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $eventId;

    /**
     * @var \AppBundle\Entity\Session
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Session")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sessionId", referencedColumnName="id",onDelete="CASCADE")
     * })
     */
    private $session;

    /**
     * @var string
     *
     * @ORM\Column(name="sponsorName", type="string", length=120, nullable=true)
     */
    private $sponsorName;

    /**
     * @var string
     *
     * @ORM\Column(name="sponsorImage", type="string", length=120, nullable=true)
     */
    private $sponsorImage;


    /**
     * Set eventId
     *
     * @param \AppBundle\Entity\Events $eventId
     *
     * @return EventCicles
     */
    public function setEventId($eventId)
    {
        $this->eventId = $eventId;

        return $this;
    }

    /**
     * Get eventId
     *
     * @return integer
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * Set session
     *
     * @param \AppBundle\Entity\Session $session
     *
     * @return EventCicles
     */
    public function setSession(\AppBundle\Entity\Session $session)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session
     *
     * @return \AppBundle\Entity\Session
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * Set sponsorName
     *
     * @param string $sponsorName
     *
     * @return EventCineforum
     */
    public function setSponsorName($sponsorName)
    {
        $this->sponsorName = $sponsorName;

        return $this;
    }

    /**
     * Get sponsorName
     *
     * @return string
     */
    public function getSponsorName()
    {
        return $this->sponsorName;
    }

    /**
     * Set sponsorImage
     *
     * @param string $sponsorImage
     *
     * @return EventCineforum
     */
    public function setSponsorImage($sponsorImage)
    {
        $this->sponsorImage = $sponsorImage;

        return $this;
    }

    /**
     * Get sponsorImage
     *
     * @return string
     */
    public function getSponsorImage()
    {
        return $this->sponsorImage;
    }
}
