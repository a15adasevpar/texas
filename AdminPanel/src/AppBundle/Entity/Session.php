<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Session
 *
 * @ORM\Table(name="session", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})}, indexes={@ORM\Index(name="billboard_id", columns={"billboard_id"})})
 * @ORM\Entity
 */
class Session
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Billboard
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Billboard")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="billboard_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $billboard;

    /**
     * @var integer
     *
     * @ORM\Column(name="hall", type="integer", nullable=false)
     */
    private $hall;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="showtime", type="time", nullable=false)
     */
    private $showtime;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set billboard
     *
     * @param \AppBundle\Entity\Billboard $billboard
     *
     * @return Session
     */
    public function setBillboard(\AppBundle\Entity\Billboard $billboard = null)
    {
        $this->billboard = $billboard;

        return $this;
    }

    /**
     * Get billboard
     *
     * @return \AppBundle\Entity\Billboard
     */
    public function getBillboard()
    {
        return $this->billboard;
    }

    /**
     * Set hall
     *
     * @param integer $hall
     *
     * @return Session
     */
    public function setHall($hall)
    {
        $this->hall = $hall;

        return $this;
    }

    /**
     * Get hall
     *
     * @return integer
     */
    public function getHall()
    {
        return $this->hall;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Session
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set showtime
     *
     * @param \DateTime $showtime
     *
     * @return Session
     */
    public function setShowtime($showtime)
    {
        $this->showtime = $showtime;

        return $this;
    }

    /**
     * Get showtime
     *
     * @return \DateTime
     */
    public function getShowtime()
    {
        return $this->showtime;
    }
}
