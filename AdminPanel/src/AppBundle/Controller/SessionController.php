<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Film;
use AppBundle\Entity\Billboard;
use AppBundle\Entity\Session;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class SessionController extends Controller
{
	 /**
     * @Route("/showSessions", name="showSessions")
     */
	public function ShowSessions(Request $request)
    {
		
		 $em = $this->getDoctrine()->GetManager();
        $query = $em->createQuery(
            'select s.id,s.date,s.showtime,s.hall,b.id as billboard,f.name
            from AppBundle:Session s left join AppBundle:Billboard b where (b.id=s.billboard) left join AppBundle:Film f where (f.id=b.film)
            order by b.id asc'
            );

        $sessions= $query->getResult();
		
		 if (count($sessions)==0) {
            return $this->render('default/message.html.twig', array(
                'message' => 'No hem trobat cap sessió'));
        }
		
		return $this->render('default/showSessions.html.twig',array(
		'sessions' => $sessions));
	}
	
		/**
	 * @param Billboard $id
	 *
	 * @Route("/{id}/insertSession", requirements={"id" = "\d+"}, name="insertSession")
	 * @return RedirectResponse
	 *
	 */
  public function insertSession(Billboard $id,Request $request){
		
		$em = $this->getDoctrine()->getManager();

		$selectedBillboard = $em->getRepository('AppBundle:Billboard')->findOneById($id);
		
		$query = $em->createQuery('select s from AppBundle:Session s where s.billboard=:param1')->setParameters(array('param1'=> $id));
		
		$sessionsExist=0;
		
		$queryResult=$query->getResult();
		
		if($queryResult)
			$sessionsExist=1;
		
		$idx=$selectedBillboard->getId();
		$filmId = $selectedBillboard->getFilm();
		
		$selectedFilm = $em->getRepository('AppBundle:Film')->findOneById($filmId);
		
		$dStart=$selectedBillboard->getDateStart();
		$dEnd=$selectedBillboard->getDateEnd();
		$dateDiff=$dStart->diff($dEnd)->format("%a");

		$defaultData = array('message' => '');
		
		$form = $this->createFormBuilder($defaultData)
			->add('date', DateType::class, array(
			'label' => false,
			'data' => $dStart,
			'html5' => false,
			'widget' => 'single_text',
			'attr' => [
			'class' => 'form-control input-inline datepicker',
			'data-provide' => 'datepicker',
			'data-date-format' => 'yyyy-mm-dd',
			]))
			->add('time', TextType::class, array('label' => false, 'data' => '16:00'))
			->add('select', ChoiceType::class, array(
			'label' => false,
			'choices' => array(
			'1' => 1,
			'2' => 2,
			'3' => 3,
			'4' => 4,
			)
		))
			->add('save',SubmitType::class)
			->getForm();
			
		$form->handleRequest($request);
		
		if ($form->isSubmitted() && $form->isValid()) {
			// data is an array with "name", "email", and "message" keys

			$data = $_REQUEST;
			
			foreach ($data as $key => $value) {
				if (is_array($value))
				{
					foreach ($value as $key2 => $value2) {
						if ($key2!="_token" && $key2!="save"){
						$word = $key2;
						$number = 0;
						$arrayasociativo[$word][$number]=$value2;
						}
					}
					continue;
				}
				$subkey=explode("_",$key);
				$word = $subkey[0];
				$number = $subkey[1];
				$arrayasociativo[$word][$number]=$value;
			}
			
			$length = max(array_map('count', $arrayasociativo));
			
			//exit(\Doctrine\Common\Util\Debug::dump(${$length}));
			foreach ($arrayasociativo[$word] as $key => $valor)
			{
				${"session_".$key} = new Session();
				${"session_".$key}->setBillboard($selectedBillboard);
				${"session_".$key}->setDate(\DateTime::createFromFormat('Y-m-d',$arrayasociativo["date"][$key]));
				${"session_".$key}->setShowtime(\DateTime::createFromFormat('H:i',$arrayasociativo["time"][$key]));
				${"session_".$key}->setHall($arrayasociativo["select"][$key]);
				
				$em = $this->getDoctrine()->getManager();
				  
				$em->persist(${"session_".$key});
				
				$em->flush(${"session_".$key});
			}
			//$i=$i-1;
			//exit(\Doctrine\Common\Util\Debug::dump(${"session_".$i}));
			return $this->redirectToRoute('selectedSessions',array('id' => $idx));
		}
		return $this->render('default/insertSession.html.twig', array(
        'form' => $form->createView(),
		'billboard' => $selectedBillboard,
		'session' => 1,
		'dateDiff' => $dateDiff,
		'film' => $selectedFilm,
		'sessionsExist' => $sessionsExist,
        ));
	}
	
	/**
     * @Route("/deleteSession", name="deleteSession")
     */
    public function deleteSession(Request $request){

    	$id=$request->request->get('data');
		
    	$em = $this->getDoctrine()->getManager();

	   $session = $em->getRepository('AppBundle:Session')->findOneById($id);


		$em->remove($session);
		$em->flush();

    	$response = new Response();

    	return $response;
    }
	
	/**
	 * @param Session $id
	 *
	 * @Route("/{id}/modifySession", requirements={"id" = "\d+"}, name="modifySession")
	 * @return RedirectResponse
	 *
	 */
    public function modifySession(Session $id,Request $request){

		$em = $this->getDoctrine()->getManager();

	   $selectedSession = $em->getRepository('AppBundle:Session')->findOneById($id);
	   $datex= $selectedSession->getDate()->format("%a");

    	$form = $this->createFormBuilder($selectedSession)
			->add('date', DateType::class, array(
			'label' => false,
			'html5' => false,
			'widget' => 'single_text',
			'attr' => [
			'class' => 'form-control input-inline datepicker',
			'data-provide' => 'datepicker',
			'data-date-format' => 'yyyy-mm-dd',
			]))
			->add('showtime', TimeType::class, array('label' => false))
			->add('hall', ChoiceType::class, array(
			'label' => false,
			'choices' => array(
			'1' => 1,
			'2' => 2,
			'3' => 3,
			'4' => 4,
			)
		))
			->add('save',SubmitType::class)
			->getForm();

    	$form->handleRequest($request);

    	if ($form->isSubmitted() && $form->isValid()) {

        	$em = $this->getDoctrine()->GetManager();

        	$em->persist($selectedSession);
        	$em->flush();

			return $this->redirectToRoute('showSessions');
	    }
        return $this->render('default/modifySession.html.twig', array(
            'title' => 'Modificar Sessió',
            'form' => $form->createView(),

        ));
    }
		/**
	 * @param Billboard $id
	 *
	 * @Route("/{id}/selectedSessions", requirements={"id" = "\d+"}, name="selectedSessions")
	 * @return RedirectResponse
	 *
	 */
	public function selectedSessions(Billboard $id, Request $request)
    {
		 $em = $this->getDoctrine()->GetManager();
        $query = $em->createQuery(
            'select s.id,s.date,s.showtime,s.hall,f.name,b.id as billboard
            from AppBundle:Session s left join AppBundle:Billboard b where (b.id=s.billboard) left join AppBundle:Film f where (f.id=b.film)
            where b.id=:param1 order by b.id asc')->setParameters(array('param1'=>$id));

        $sessions= $query->getResult();
		
		$query2=$em->createQuery('select f.name from AppBundle:Billboard b left join AppBundle:Film f where (b.film=f.id) where b.id=:param1 group by b.id')->setParameters(array('param1'=>$id));
		
		$name=$query2->getResult();
		
		 if (count($sessions)==0) {
            return $this->render('default/message.html.twig', array(
                'message' => 'No hem trobat cap sessió'));
        }
		
		return $this->render('default/showSelectedSessions.html.twig',array(
		'sessions' => $sessions,
		'id' => $id,
		'names' =>$name,));
	}
	
		/**
	 * @param Billboard $id
	 *
	 * @Route("/{id}/deleteAllSessions", requirements={"id" = "\d+"}, name="deleteAllSessions")
	 * @return RedirectResponse
	 *
	 */
    public function deleteAllSessions(Billboard $id,Request $request){
		
    	$em = $this->getDoctrine()->getManager();
		//exit(\Doctrine\Common\Util\Debug::dump(${$id}));
		$query=$em->createQuery('delete from AppBundle:Session s where s.billboard=:param1')->setParameters(array('param1'=>$id));

		$query->execute();


    	return $this->redirectToRoute('showBillboard');
    }
}
