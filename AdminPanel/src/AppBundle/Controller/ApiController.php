<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\User;

class ApiController extends FOSRestController
{

    /**
     * @Rest\Get("/api")
     */
    public function getApiAction()
    {
		$em = $this->getDoctrine()->GetManager();

		$query = $em->createQuery('select s from AppBundle:Session s order by s.id asc');


		$results= $query->getResult();


        if ($results === null) {
          return new View("there are no users exist", Response::HTTP_NOT_FOUND);
     }
        return $results;
    }

    /**
     * @Rest\Get("/apiEsdevenimentsGeneric")
     */
    public function getApiEsdevenimentsGenericAction()
    {
		$em = $this->getDoctrine()->GetManager();

		$query = $em->createQuery("select e from AppBundle:Events e where e.type='Generic'");

		$results= $query->getResult();

        if ($results === null) {
          return new View("there are no users exist", Response::HTTP_NOT_FOUND);
     }
        return $results;
    }

    /**
     * @Rest\Get("/apiEsdevenimentsCicles")
     */
    public function getApiEsdevenimentsCiclesAction()
    {
		$em = $this->getDoctrine()->GetManager();

		$query = $em->createQuery('select c from AppBundle:EventCicles c');

		$results= $query->getResult();

        if ($results === null) {
          return new View("there are no users exist", Response::HTTP_NOT_FOUND);
     }
        return $results;
    }

    /**
     * @Rest\Get("/apiEsdevenimentsCineforum")
     */
    public function getApiEsdevenimentsCineforumAction()
    {
		$em = $this->getDoctrine()->GetManager();

		$query = $em->createQuery('select ci from AppBundle:EventCineforum ci');

		$results= $query->getResult();

        if ($results === null) {
          return new View("there are no users exist", Response::HTTP_NOT_FOUND);
     }
        return $results;
    }

    /**
     * @Rest\Get("/apiComingSoon")
     */
    public function getApiComingSoonAction()
    {
		$em = $this->getDoctrine()->GetManager();

		$query = $em->createQuery('select c from AppBundle:Comingsoon c');

		$results= $query->getResult();

        if ($results === null) {
          return new View("there are no users exist", Response::HTTP_NOT_FOUND);
     }
        return $results;
    }
}
