<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Film;
use AppBundle\Entity\Billboard;
use AppBundle\Entity\Session;
use AppBundle\Entity\Events;
use AppBundle\Entity\EventCicles;
use AppBundle\Entity\EventCineforum;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class EventsController extends Controller
{
	 /**
     * @Route("/showEvents", name="showEvents")
     */
	public function ShowEvents(Request $request)
    {
		
		 $em = $this->getDoctrine()->GetManager();
        $query = $em->createQuery(
            'select e
            from AppBundle:Events e order by e.id asc');

        $events= $query->getResult();
		
		 if (count($events)==0) {
            return $this->render('default/message.html.twig', array(
                'message' => 'No hem trobat cap event'));
        }
		
		return $this->render('default/showEvents.html.twig',array(
		'events' => $events));
	}
	

    /**
     * @Route("/insertEvents", name="insertEvents")
     */
    public function InsertEvents(Request $request)
    {
		
		$defaultData = array('message' => '');
		
		$form = $this->createFormBuilder($defaultData)
		->add('film', EntityType::class, array(
    		'class' => 'AppBundle:Film',
			'placeholder' => 'Escull una pel·lícula',
			'empty_data'  => null,
    		'choice_label' => 'name',
			'required' => false))
		->add('film2', EntityType::class, array(
    		'class' => 'AppBundle:Film',
    		'choice_label' => 'name',
			'placeholder' => 'Escull una pel·lícula',
			'empty_data'  => null,
			'required' => false))
		->add('film3', EntityType::class, array(
    		'class' => 'AppBundle:Film',
    		'choice_label' => 'name',
			'placeholder' => 'Escull una pel·lícula',
			'empty_data'  => null,
			'required' => false))
		->add('film4', EntityType::class, array(
    		'class' => 'AppBundle:Film',
    		'choice_label' => 'name',
			'placeholder' => 'Escull una pel·lícula',
			'empty_data'  => null,
			'required' => false))
		->add('session', EntityType::class, array(
    		'class' => 'AppBundle:Session',
    		'choice_label' => 'id',
			'placeholder' => 'Escull una sessió',
			'empty_data'  => null,
			'required' => false))
		->add('sponsorImg', FileType::class, array(
			'required' => false))
		->add('sponsorName', TextType::class, array(
			'required' => false))
		->add('save',SubmitType::class)
		->getForm();
			
		$form->handleRequest($request);
		
    	if ($form->isSubmitted() && $form->isValid()) {
			
			$em = $this->getDoctrine()->GetManager();
			$formData=$form->getData();
			
			$event = new Events();
			
			$name=$request->request->get('name');
			$description=$request->request->get('description');
			$type=$request->request->get('type');
			$dateStart=$request->request->get('dateStart');
			$dateEnd=$request->request->get('dateEnd');
			
			$event->setName($name);
			$event->setDescription($description);
			$event->setType($type);
			$event->setDateStart(\DateTime::createFromFormat('Y-m-d',$dateStart));
			$event->setDateEnd(\DateTime::createFromFormat('Y-m-d',$dateEnd));
			
			$em->persist($event);
			$em->flush();
			
			$id=$event->getId();
			
			$eventId=$em->getRepository('AppBundle:Events')->findOneById($id);

			if($type=="Cineforum"){
				
				/** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
				$file = $formData['sponsorImg'];
				$fileName = md5(uniqid()).'.'.$file->guessExtension();
				// Move the file to the directory where brochures are stored
				$file->move($this->getParameter('films_directory'),$fileName);
				
				$eventCineforum = new EventCineforum();
				
				$session=$formData['session'];
				
				$selectedSession = $em->getRepository('AppBundle:Session')->findOneById($session);
				
				$sponsorName=$request->request->get('sponsorName');
				$eventCineforum->setEventId($eventId);
				$eventCineforum->setSession($selectedSession);
				$eventCineforum->setSponsorImage($fileName);
				$eventCineforum->setSponsorName($sponsorName);
				
				$em->persist($eventCineforum);
				$em->flush();
			}elseif($type=="Cicle"){
				
				$film1 = $em->getRepository('AppBundle:Film')->findOneById($formData['film']);
				$film2 = $em->getRepository('AppBundle:Film')->findOneById($formData['film2']);
				$film3 = $em->getRepository('AppBundle:Film')->findOneById($formData['film3']);
				$film4 = $em->getRepository('AppBundle:Film')->findOneById($formData['film4']);
				
				$ec1 = new EventCicles();
				$ec2 = new EventCicles();
				$ec3 = new EventCicles();
				$ec4 = new EventCicles();
				
				if(!empty($film1)){
					$ec1->setEventId($eventId);
					$ec1->setFilm($film1);
					$em->persist($ec1);
				}
				if(!empty($film2)){
					$ec2->setEventId($eventId);
					$ec2->setFilm($film2);
					$em->persist($ec2);
				}
				if(!empty($film3)){
					$ec3->setEventId($eventId);
					$ec3->setFilm($film3);
					$em->persist($ec3);
				}
				if(!empty($film4)){
					$ec4->setEventId($eventId);
					$ec4->setFilm($film4);
					$em->persist($ec4);
				}
				$em->flush();
			}
		
			return $this->redirectToRoute('showEvents');
	    }
        return $this->render('default/insertEvents.html.twig', array(
			'form' => $form->createView(),

        ));
    }
	
	
	/**
     * @Route("/deleteEvent", name="deleteEvent")
     */
    public function deleteEvent(Request $request){

    	$id=$request->request->get('data');
		
    	$em = $this->getDoctrine()->getManager();

	   $event = $em->getRepository('AppBundle:Events')->findOneById($id);


		$em->remove($event);
		$em->flush();

    	$response = new Response();

    	return $response;
    }
}
