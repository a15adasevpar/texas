<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Film;
use AppBundle\Entity\Billboard;
use AppBundle\Entity\Session;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FilmController extends Controller
{
    /**
     * @Route("/insertFilm", name="insertFilm")
     */
    public function InsertFilm(Request $request)
    {

    	$film = new Film();

    	   $form = $this->createFormBuilder($film)
            ->add('name', TextType::class)
            ->add('overview', TextareaType::class)
            ->add('awards', TextareaType::class)
            ->add('originalTitle', TextType::class)
            ->add('nacionality', TextType::class)
            ->add('productionYear', NumberType::class)
            ->add('genre', TextType::class)
            ->add('duration', NumberType::class)
            ->add('directors', TextareaType::class)
            ->add('actors', TextareaType::class)
            ->add('rate', ChoiceType::class, array(
            	'choices' => array(
					'Tothom' => 0,
            		'7' => 7,
            		'12' => 12,
            		'16' => 16,
            		'18' => 18,
            		)
            	))
            ->add('version', TextType::class)
            ->add('image', FileType::class)
			->add('trailer', TextType::class)
			->add('save', SubmitType::class, array('label' => 'Inserir'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

        	$em = $this->getDoctrine()->GetManager();

        	 /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $film->getImage();

            $fileName = md5(uniqid()).'.'.$file->guessExtension();


            // Move the file to the directory where brochures are stored
            $file->move(
                $this->getParameter('films_directory'),
                $fileName
            );

            $film->setImage($fileName);

			$em->persist($film);

			$em->flush();

		return $this->redirectToRoute('showFilms');
	    }
        return $this->render('default/insertFilm.html.twig', array(
            'form' => $form->createView(),
        ));
    }
	
	 /**
     * @Route("/showFilms", name="showFilms")
     */
	public function ShowFilmsAction(Request $request)
    {
		
		 $em = $this->getDoctrine()->GetManager();
        $query = $em->createQuery(
            'select f
            from AppBundle:Film f
            order by f.id asc'
            );

        $films= $query->getResult();
		
		 if (count($films)==0) {
            return $this->render('default/message.html.twig', array(
                'message' => 'No hem trobat cap pel·lícula'));
        }
		
		return $this->render('default/showFilms.html.twig',array(
		'films' => $films));
	}
	
	 /**
     * @Route("/deleteFilm", name="deleteFilm")
     */
    public function deleteFilm(Request $request){

    	$id=$request->request->get('data');
		
    	$em = $this->getDoctrine()->getManager();

	   $film = $em->getRepository('AppBundle:Film')->findOneById($id);


		$em->remove($film);
		$em->flush();

    	$response = new Response();

    	return $response;
    }
	
		/**
	 * @param Film $id
	 *
	 * @Route("/{id}/modifyFilm", requirements={"id" = "\d+"}, name="modifyFilm")
	 * @return RedirectResponse
	 *
	 */
    public function modifyFilm(Film $id,Request $request){
		
		$em = $this->getDoctrine()->getManager();

		$selectedFilm = $em->getRepository('AppBundle:Film')->findOneById($id);
		
		   $form = $this->createFormBuilder($selectedFilm)
            ->add('name', TextType::class)
            ->add('overview', TextareaType::class)
            ->add('awards', TextareaType::class)
            ->add('originalTitle', TextType::class)
            ->add('nacionality', TextType::class)
            ->add('productionYear', NumberType::class)
            ->add('genre', TextType::class)
            ->add('duration', NumberType::class)
            ->add('directors', TextareaType::class)
            ->add('actors', TextareaType::class)
            ->add('rate', ChoiceType::class, array(
            	'choices' => array(
            		'Tothom' => 0,
            		'7' => 7,
            		'12' => 12,
            		'16' => 16,
            		'18' => 18,
            		)
            	))
            ->add('version', TextType::class)
            ->add('image', FileType::class, array('data_class' => null))
			->add('trailer', TextType::class)
			->add('save', SubmitType::class, array('label' => 'Inserir'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

        	$em = $this->getDoctrine()->GetManager();

        	 /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $selectedFilm->getImage();

            $fileName = md5(uniqid()).'.'.$file->guessExtension();


            // Move the file to the directory where brochures are stored
            $file->move(
                $this->getParameter('films_directory'),
                $fileName
            );

            $selectedFilm->setImage($fileName);

			$em->persist($selectedFilm);

			$em->flush();

		return $this->redirectToRoute('showFilms');
	    }
        return $this->render('default/modifyFilm.html.twig', array(
            'form' => $form->createView(),
        ));
	}
}
