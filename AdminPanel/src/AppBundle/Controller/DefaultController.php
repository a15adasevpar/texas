<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
       $em = $this->getDoctrine()->GetManager();
        $query = $em->createQuery(
            'select f
            from AppBundle:Film f
            order by f.id asc'
            );

        $films= $query->getResult();
		
		 if (count($films)==0) {
            return $this->render('default/message.html.twig', array(
                'message' => 'No hem trobat cap pel·lícula'));
        }

       return $this->render('default/showFilms.html.twig',array(
		'films' => $films));
    }
}
