<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Film;
use AppBundle\Entity\Billboard;
use AppBundle\Entity\Session;
use AppBundle\Entity\Comingsoon;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ComingSoonController extends Controller
{
	 /**

     * @Route("/showComingSoon", name="showComingSoon")
     */
	public function showComingSoon(Request $request)
    {
		
		 $em = $this->getDoctrine()->GetManager();
        $query = $em->createQuery(
            'select c.id,c.dateStart,c.dateEnd,f.name
            from AppBundle:Comingsoon c join AppBundle:Film f where (f.id=c.film)
            order by c.id asc'
            );

        $comingsoon= $query->getResult();
		
		 if (count($comingsoon)==0) {
            return $this->render('default/message.html.twig', array(
                'message' => 'No hem trobat cap pel·lícula a properament'));
        }
		
		return $this->render('default/showComingSoon.html.twig',array(
		'comingsoon' => $comingsoon));
	}
	
    /**
     * @Route("/insertComingSoon", name="insertComingSoon")
     */
    public function InsertComingSoon(Request $request)
    {
    	$comingsoon = new Comingsoon();

    	$form = $this->createFormBuilder($comingsoon)
    	->add('film', EntityType::class, array(
    		'class' => 'AppBundle:Film',
    		'choice_label' => 'name',))
    	->add('dateStart', DateType::class, array(
				'widget' => 'single_text',
				'html5' => false,
				'attr' => [
        'class' => 'form-control input-inline datepicker',
        'data-provide' => 'datepicker',
        'data-date-format' => 'yyyy-mm-dd',
    ]))
		->add('dateEnd', DateType::class, array(
				'widget' => 'single_text',
				'html5' => false,
				'attr' => [
        'class' => 'form-control input-inline datepicker',
        'data-provide' => 'datepicker',
        'data-date-format' => 'yyyy-mm-dd',
    ]))
		->add('save', SubmitType::class, array('label' => 'Inserir'))
    	->getForm();

    	$form->handleRequest($request);

    	if ($form->isSubmitted() && $form->isValid()) {

        	$em = $this->getDoctrine()->GetManager();

        	$em->persist($comingsoon);
        	$em->flush();

			return $this->redirectToRoute('showComingSoon');
	    }
        return $this->render('default/insertComingSoon.html.twig', array(
            'title' => 'Inserir Properament',
            'form' => $form->createView(),

        ));
    }
	
	 /**
     * @Route("/deleteComingSoon", name="deleteComingSoon")
     */
    public function deleteComingSoon(Request $request){

    	$id=$request->request->get('data');
		
    	$em = $this->getDoctrine()->getManager();

	   $comingsoon = $em->getRepository('AppBundle:Comingsoon')->findOneById($id);


		$em->remove($comingsoon);
		$em->flush();

    	$response = new Response();

    	return $response;
    }
	
	/**
	 * @param Comingsoon $id
	 *
	 * @Route("/{id}/modifyComingSoon", requirements={"id" = "\d+"}, name="modifyComingSoon")
	 * @return RedirectResponse
	 *
	 */
    public function modifyComingSoon(Comingsoon $id,Request $request){

		$em = $this->getDoctrine()->getManager();

	   $selectedComingSoon = $em->getRepository('AppBundle:ComingSoon')->findOneById($id);

    	$form = $this->createFormBuilder($selectedComingSoon)
    	->add('film', EntityType::class, array(
    		'class' => 'AppBundle:Film',
    		'choice_label' => 'name',))
    	->add('dateStart', DateType::class, array(
				'widget' => 'single_text',
				'html5' => false,
				'attr' => [
        'class' => 'form-control input-inline datepicker',
        'data-provide' => 'datepicker',
        'data-date-format' => 'yyyy-mm-dd',
    ]))
		->add('dateEnd', DateType::class, array(
				'widget' => 'single_text',
				'html5' => false,
				'attr' => [
        'class' => 'form-control input-inline datepicker',
        'data-provide' => 'datepicker',
        'data-date-format' => 'yyyy-mm-dd',
    ]))
		->add('save', SubmitType::class, array('label' => 'Modificar'))
    	->getForm();

    	$form->handleRequest($request);

    	if ($form->isSubmitted() && $form->isValid()) {

        	$em = $this->getDoctrine()->GetManager();

        	$em->persist($selectedComingSoon);
        	$em->flush();

			return $this->redirectToRoute('showComingSoon');
	    }
        return $this->render('default/modifyComingSoon.html.twig', array(
            'title' => 'Modificar Properament',
            'form' => $form->createView(),

        ));
    }
}
