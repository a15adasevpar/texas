<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Film;
use AppBundle\Entity\Billboard;
use AppBundle\Entity\Session;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BillboardController extends Controller
{
	 /**

     * @Route("/showBillboard", name="showBillboard")
     */
	public function ShowBillboards(Request $request)
    {
		
		 $em = $this->getDoctrine()->GetManager();
        $query = $em->createQuery(
            'select b.id,b.dateStart,b.dateEnd,f.name,count(s) as contador
            from AppBundle:Billboard b left join AppBundle:Film f where (f.id=b.film) left join AppBundle:Session s where (b.id=s.billboard) group by b.id');

        $billboards= $query->getResult();
		
		
		 if (count($billboards)==0) {
            return $this->render('default/message.html.twig', array(
                'message' => 'No hem trobat cap pel·lícula a la cartellera'));
        }
		
		return $this->render('default/showBillboards.html.twig',array(
		'billboards' => $billboards));
	}
	
    /**
     * @Route("/insertBillboard", name="insertBillboard")
     */
    public function InsertBillboard(Request $request)
    {
    	$billboard = new Billboard();

    	$form = $this->createFormBuilder($billboard)
    	->add('film', EntityType::class, array(
    		'class' => 'AppBundle:Film',
    		'choice_label' => 'name',
			'data' => "asdadas",
			'placeholder' => "Escull una pel·lícula"))
    	->add('dateStart', DateType::class, array(
				'widget' => 'single_text',
				'html5' => false,
				'attr' => [
        'class' => 'form-control input-inline datepicker',
        'data-provide' => 'datepicker',
        'data-date-format' => 'yyyy-mm-dd',
    ]))
		->add('dateEnd', DateType::class, array(
				'widget' => 'single_text',
				'html5' => false,
				'attr' => [
        'class' => 'form-control input-inline datepicker',
        'data-provide' => 'datepicker',
        'data-date-format' => 'yyyy-mm-dd',
    ]))
		->add('save', SubmitType::class, array('label' => 'Inserir'))
    	->getForm();

    	$form->handleRequest($request);

    	if ($form->isSubmitted() && $form->isValid()) {

        	$em = $this->getDoctrine()->GetManager();

        	$em->persist($billboard);
        	$em->flush();

		$id=$billboard->getId();

			return $this->redirectToRoute('showBillboard');
	    }
        return $this->render('default/insertBillboard.html.twig', array(
            'title' => 'Inserir Cartelera',
            'form' => $form->createView(),

        ));
    }
	
	 /**
     * @Route("/deleteBillboard", name="deleteBillboard")
     */
    public function deleteBillboard(Request $request){

    	$id=$request->request->get('data');
		
    	$em = $this->getDoctrine()->getManager();

	   $billboard = $em->getRepository('AppBundle:Billboard')->findOneById($id);


		$em->remove($billboard);
		$em->flush();

    	$response = new Response();

    	return $response;
    }
	
	/**
	 * @param Billboard $id
	 *
	 * @Route("/{id}/modifyBillboard", requirements={"id" = "\d+"}, name="modifyBillboard")
	 * @return RedirectResponse
	 *
	 */
    public function modifyBillboard(Billboard $id,Request $request){

		$em = $this->getDoctrine()->getManager();

	   $selectedBillboard = $em->getRepository('AppBundle:Billboard')->findOneById($id);

    	$form = $this->createFormBuilder($selectedBillboard)
    	->add('film', EntityType::class, array(
    		'class' => 'AppBundle:Film',
    		'choice_label' => 'name',))
    	->add('dateStart', DateType::class, array(
				'widget' => 'single_text',
				'html5' => false,
				'attr' => [
        'class' => 'form-control input-inline datepicker',
        'data-provide' => 'datepicker',
        'data-date-format' => 'yyyy-mm-dd',
    ]))
		->add('dateEnd', DateType::class, array(
				'widget' => 'single_text',
				'html5' => false,
				'attr' => [
        'class' => 'form-control input-inline datepicker',
        'data-provide' => 'datepicker',
        'data-date-format' => 'yyyy-mm-dd',
    ]))
		->add('save', SubmitType::class, array('label' => 'Modificar'))
    	->getForm();

    	$form->handleRequest($request);

    	if ($form->isSubmitted() && $form->isValid()) {

        	$em = $this->getDoctrine()->GetManager();

        	$em->persist($selectedBillboard);
        	$em->flush();

			return $this->redirectToRoute('showBillboard');
	    }
        return $this->render('default/modifyBillboard.html.twig', array(
            'title' => 'Modificar Cartelera',
            'form' => $form->createView(),

        ));
    }
}
