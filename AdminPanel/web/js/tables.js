$(document).ready(function() {
        $('#example').dataTable({
         "language": {
        processing:     "Buscant...",
        search:         "Buscar:",
        lengthMenu:    "Mostrar _MENU_ entrades",
        info:           "Mostrant  _START_ a _END_ de _TOTAL_ entrades",
        infoEmpty:      "Mostrant 0 a 0 de 0 entrades",
        infoFiltered:   "(Filtrat de _MAX_ entrades totals)",
        infoPostFix:    "",
        loadingRecords: "Carregant...",
        zeroRecords:    "No s'ha trobat res",
        emptyTable:     "La taula est&agrave; buida",
        paginate: {
            first:      "Primera",
            previous:   "Anterior",
            next:       "Seg&uuml;ent",
            last:       "&Uacute;tima"
        }
} 
});
});