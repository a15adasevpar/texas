$(document).ready(function(){	
	$('.submenu').hide();

	$("li:has(ul)").click(function(){
		$('.submenu').hide();
		$("ul",this).toggle();
	});
  
	deleteBillboard();
	deleteFilm();
	deleteComingSoon();
	deleteSession();
	deleteEvents();
	insertEvents();
	var i=0;
	var k=1;
	var oldJ=2;
	var variables=[];
	variables[0]=k;
	variables[1]=oldJ;
	
	$(document).on('focusout','.hourInput', function(e){
		validate($(this).attr('id'));
	});
	
	$('#formBillboard').on('submit', function(e){
		if ($('#form_dateStart').val()>$('#form_dateEnd').val()){
			e.preventDefault();
			$('.errormsg').text("Error: La data d'inici de la cartellera no pot ser superior a la data final").show();
		}else{}
	});
	$('body').delegate('.addSessionDiv','mouseenter', function(e) { 
		$('.addSessionDiv').css('cursor', 'pointer');
	});

		
	$('body').delegate('.removeSessionDiv','mouseenter', function(e) { 
		$('.removeSessionDiv').css('cursor', 'pointer');
	});
	$('body').delegate('.removeSessionDiv','click', function(e) { 
		j=$(this).attr('id');
		deleteRow(j);
	});


	$('#formComingSoon').on('submit', function(e){
		if ($('#form_dateStart').val()>$('#form_dateEnd').val()){
			e.preventDefault();
			$('.errormsg').text("Error: La data d'inici de properament no pot ser superior a la data final").show();
		}else{}
	});

	$('#formEvents').on('submit', function(e){
		if ($('#form_dateStart').val()>$('#form_dateEnd').val()){
			e.preventDefault();
			$('.errormsg').text("Error: La data d'inici de l'esdeveniment no pot ser superior a la data final").show();
    }else{}
	});
});

function deleteBillboard(){
	$('#example').delegate('.deleteBillboard','mouseenter', function(e) { 
	$('.deleteBillboard').css('cursor', 'pointer');
	});
    $('#example').delegate('.deleteBillboard','click', function(e) { 
    	var deleteId= $(this).attr('id');
		deleteBillboardAjax(deleteId);
    });
}

function deleteFilm(){
	$('#example').delegate('.deleteFilm','mouseenter', function(e) { 
	$('.deleteFilm').css('cursor', 'pointer');
	});
    $('#example').delegate('.deleteFilm','click', function(e) { 
    	var deleteId= $(this).attr('id');
		deleteFilmAjax(deleteId);
    });
}

function deleteComingSoon(){
	$('#example').delegate('.deleteComingSoon','mouseenter', function(e) { 
	$('.deleteComingSoon').css('cursor', 'pointer');
	});
    $('#example').delegate('.deleteComingSoon','click', function(e) { 
    	var deleteId= $(this).attr('id');
		deleteComingSoonAjax(deleteId);
    });
}

function deleteSession(){
	$('#example').delegate('.deleteSession','mouseenter', function(e) { 
	$('.deleteSession').css('cursor', 'pointer');
	});
    $('#example').delegate('.deleteSession','click', function(e) { 
    	var deleteId= $(this).attr('id');
		deleteSessionAjax(deleteId);
    });
}

function deleteEvents(){
	$('#example').delegate('.deleteEvents','mouseenter', function(e) { 
	$('.deleteEvents').css('cursor', 'pointer');
	});
    $('#example').delegate('.deleteEvents','click', function(e) { 
    	var deleteId= $(this).attr('id');
		deleteEventsAjax(deleteId);
    });
}

function addForm(dateDiff,sessionsExist){

if(sessionsExist==0){
	var startDate = $("#form_date").val();
	var nextDate = new Date(startDate);
	
	for(var i=1;i<dateDiff;i++){
	
	nextDate.setDate(nextDate.getDate()+1);
	var lineDate= [nextDate.getFullYear(),nextDate.getMonth()+1,nextDate.getDate()].join('-');

	var newLine=("<fieldset id='fieldset_"+i+"'><div class='form-group col-sm-4'><label class='labels'>Dia</label><input type='text'value='"+lineDate+"' id='date_"+i+"' name='date_"+i+"' class='form-control input-inline datepicker' data-provide='datepicker' data-date-format='yyyy-mm-dd'></div><div class='form-group col-sm-4'><label class='labels'>Hora</label><input type='text' id='time_"+i+"' name='time_"+i+"' class='form-control hourInput' value='16:00'></div><div class='form-group col-sm-2 addHallDiv'><label class='labels'>Sala</label><select name='select_"+i+"' class='form-control'><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option></select></div><div class='form-group col-sm-1 addSessionDiv' title='Afegir Sessió' id='"+i+"'><span class='glyphicon glyphicon-plus-sign addSession'></span></div><div class='form-group col-sm-1 removeSessionDiv' title='Esborrar Sessió' id='"+i+"'><span class='glyphicon glyphicon-minus-sign addSession'></span></div></fieldset>");
	  $("#form0").append(newLine);
	}
	$("#form0").append($("#form_save"));
	}
	else{}
	
	return i;
}

function addRow(i,j,k,oldJ){
	
	var time="16:00";
	
	k++;
	if(k>4){k=1;}

	switch(k){
		case 1: time="16:00";
		break;
		case 2: time="18:00";
		break;
		case 3: time="20:00";
		break;
		case 4: time="22:00";
		break;
	}
	
	var newrow=("<fieldset id='fieldset_"+i+"' class='newrow highlight'><div class='form-group col-sm-4'><label class='labels'>Dia</label><input type='text'id='date_"+i+"' name='date_"+i+"' class='form-control input-inline datepicker' data-provide='datepicker' data-date-format='yyyy-mm-dd'></div><div class='form-group col-sm-4'><label class='labels'>Hora</label><input id='time_"+i+"' type='text' name='time_"+i+"' class='form-control hourInput' value='"+time+"'></div><div class='form-group col-sm-2 addHallDiv'><label class='labels'>Sala</label><select name='select_"+i+"' id='select_"+i+"' class='form-control'><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option></select></div></div><div class='form-group col-sm-1 addSessionDiv' title='Afegir Sessió' id='"+i+"'><span class='glyphicon glyphicon-plus-sign addSession'></span></div><div class='form-group col-sm-1 removeSessionDiv' title='Esborrar Sessió' id='"+i+"'><span class='glyphicon glyphicon-minus-sign addSession'></span></div></fieldset>");
	if(j==0){
		var value = $('#form_date').val();
		$('#fieldset_'+j).after(newrow);
		$('#date_'+i).val(value);
		$('#select_'+i).val(k);
	}else{
		var value = $('#date_'+j).val();
		$('#fieldset_'+j).after(newrow);
		$('#date_'+i).val(value);
		$('#select_'+i).val(k);
	}
	oldJ = j;
	i++;
	return [i,k,oldJ];

}

function deleteRow(j){
	$('#fieldset_'+j).remove();
}

function insertEvents(){
	$('#eventType').on('change',function(){
		var selection = $(this).val();
		switch(selection){
		case "Cicle":
		$("#cineforumType").hide();
		$("#cicleType").show();
	   break;
		case "Cineforum":
		$("#cicleType").hide();
		$("#cineforumType").show();
		break;
		default:
		 $("#cicleType").hide();
		 $("#cineforumType").hide();
		}
	});
}

function validate(id) {
    var n=document.getElementById(id);
    var re=/^([0-1][0-9]|2[0-3]):([0-5][0-9])$/;
	var placeholder="Introdueix un format hh:mm"
if (n != null) {
    if(re.test(n.value))
    {
		$("#form_save").show();
		$("#noSend").hide();
		n.style.backgroundColor="white";
		n.style.color="black";
		n.style.fontWeight="normal";
    }
    else
    {	
        n.style.backgroundColor="#F40C0C";
		n.style.color="white";
		n.style.fontWeight="bold";
        n.focus();
		$("#form_save").hide();
		$("#noSend").show();
    }
}
}
